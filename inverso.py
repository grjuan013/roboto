import math as mt
import operaciones as op

""" px = -12.14787484
py = -10.04312125
pz = -5.07743783

x_euler = -176.38355843
y_euler = -19.68349808
z_euler = 160.62758414

l0 = 10
l1 = 12
l2 = 12
l4 = 13
l5 = 15 """


class datos:
    def __init__(self, x_euler, y_euler, z_euler, px, py, pz, l0, l1, l2, l4, l5):
        self.x_euler = x_euler
        self.y_euler = y_euler
        self.z_euler = z_euler
        self.px = px
        self.py = py
        self.pz = pz
        self.eulerxyz = op.rotate('x', self.x_euler, 'degrees') * op.rotate('y', self.y_euler, 'degrees') * op.rotate(
            'z', self.z_euler, 'degrees')
        self.nx = self.eulerxyz[0, 0]
        self.ny = self.eulerxyz[1, 0]
        self.nz = self.eulerxyz[2, 0]
        self.ox = self.eulerxyz[0, 1]
        self.oy = self.eulerxyz[1, 1]
        self.oz = self.eulerxyz[2, 1]
        self.ax = self.eulerxyz[0, 2]
        self.ay = self.eulerxyz[1, 2]
        self.az = self.eulerxyz[2, 2]
        self.l0 = l0
        self.l1 = l1
        self.l2 = l2
        self.l4 = l4
        self.l5 = l5
        self.pmx = self.px-(self.l4+self.l5)*self.ax
        self.pmy = self.py-(self.l4+self.l5)*self.ay
        self.pmz = self.pz-(self.l4+self.l5)*self.az


def teta1(d):
    result = {}
    result[0] = mt.atan(d.pmy/d.pmx)-mt.atan(-d.l2 /
                                             (mt.sqrt(d.pmx**2 + d.pmy**2 - d.l2**2)))
    result[1] = mt.atan(d.pmy/d.pmx)-mt.atan(-d.l2 /
                                             -(mt.sqrt(d.pmx**2 + d.pmy**2 - d.l2**2)))
    return result


def teta2(d, tetauno):
    result = {}
    result[0] = mt.atan(
        (mt.cos(tetauno[0])*d.pmx + mt.sin(tetauno[0])*d.pmy)/((d.l0+d.l1)-d.pmz)) + mt.pi/2
    result[1] = mt.atan(
        (mt.cos(tetauno[1])*d.pmx + mt.sin(tetauno[1])*d.pmy)/((d.l0+d.l1)-d.pmz)) - mt.pi/2
    return result


def d3(d, tetauno, tetados):
    result = {}
    result[0] = (mt.cos(tetauno[0])*d.pmx + mt.sin(tetauno[0])
                 * d.pmy)/mt.sin(tetados[0] + mt.pi/2)  # 0 0
    result[1] = (mt.cos(tetauno[1])*d.pmx + mt.sin(tetauno[1])
                 * d.pmy)/mt.sin(tetados[0] + mt.pi/2)  # 1 0
    result[2] = (mt.cos(tetauno[1])*d.pmx + mt.sin(tetauno[1])
                 * d.pmy)/mt.sin(tetados[1] + mt.pi/2)  # 1 1
    result[3] = (mt.cos(tetauno[0])*d.pmx + mt.sin(tetauno[0])
                 * d.pmy)/mt.sin(tetados[1] + mt.pi/2)  # 0 1
    return result


def teta4(d, tetauno, tetados):
    result = {}
    result[0] = mt.acos(d.ax * mt.cos(tetauno[0])*mt.sin(tetados[0]+mt.pi/2)-d.az *
                        mt.cos(tetados[0]+mt.pi/2)+d.ay*mt.sin(tetauno[0]*mt.sin(tetados[0]+mt.pi/2)))  # 0 0
    result[1] = mt.acos(d.ax * mt.cos(tetauno[1])*mt.sin(tetados[0]+mt.pi/2)-d.az *
                        mt.cos(tetados[0]+mt.pi/2)+d.ay*mt.sin(tetauno[1]*mt.sin(tetados[0]+mt.pi/2)))  # 1 0
    result[2] = -mt.acos(d.ax * mt.cos(tetauno[0])*mt.sin(tetados[0]+mt.pi/2)-d.az *
                         mt.cos(tetados[0]+mt.pi/2)+d.ay*mt.sin(tetauno[0]*mt.sin(tetados[0]+mt.pi/2)))  # - 0 0
    result[3] = -mt.acos(d.ax * mt.cos(tetauno[1])*mt.sin(tetados[0]+mt.pi/2)-d.az *
                         mt.cos(tetados[0]+mt.pi/2)+d.ay*mt.sin(tetauno[1]*mt.sin(tetados[0]+mt.pi/2)))  # - 1 0

    result[4] = mt.acos(d.ax * mt.cos(tetauno[0])*mt.sin(tetados[1]+mt.pi/2)-d.az *
                        mt.cos(tetados[1]+mt.pi/2)+d.ay*mt.sin(tetauno[0]*mt.sin(tetados[1]+mt.pi/2)))  # 0 1
    result[5] = mt.acos(d.ax * mt.cos(tetauno[1])*mt.sin(tetados[1]+mt.pi/2)-d.az *
                        mt.cos(tetados[1]+mt.pi/2)+d.ay*mt.sin(tetauno[1]*mt.sin(tetados[1]+mt.pi/2)))  # 1 1
    result[6] = -mt.acos(d.ax * mt.cos(tetauno[0])*mt.sin(tetados[1]+mt.pi/2)-d.az *
                         mt.cos(tetados[1]+mt.pi/2)+d.ay*mt.sin(tetauno[0]*mt.sin(tetados[1]+mt.pi/2)))  # - 0 1
    result[7] = -mt.acos(d.ax * mt.cos(tetauno[1])*mt.sin(tetados[1]+mt.pi/2)-d.az *
                         mt.cos(tetados[1]+mt.pi/2)+d.ay*mt.sin(tetauno[1]*mt.sin(tetados[1]+mt.pi/2)))  # - 1 1

    return result


def teta5(d, tetauno):
    result = {}
    result[0] = -mt.asin(mt.sin(tetauno[0])*d.nx - mt.cos(tetauno[0])*d.ny)
    result[1] = -mt.asin(mt.sin(tetauno[1])*d.nx - mt.cos(tetauno[1])*d.ny)
    result[2] = mt.asin(mt.sin(tetauno[0])*d.nx - mt.cos(tetauno[0])*d.ny)
    result[3] = mt.asin(mt.sin(tetauno[1])*d.nx - mt.cos(tetauno[1])*d.ny)
    return result


def r2d(angles):
    result = {k: mt.degrees(v) for k, v in angles.items()}
    return result


def lista(t1, t2, d3, t4, t5):
    result = {}
    result[0] = [t1[0], t2[0], d3[0], t4[0], t5[0]]
    result[1] = [t1[0], t2[0], d3[0], t4[2], t5[0]]
    result[2] = [t1[0], t2[0], d3[0], t4[0], t5[2]]
    result[3] = [t1[0], t2[0], d3[0], t4[2], t5[2]]

    result[4] = [t1[1], t2[0], d3[1], t4[1], t5[1]]
    result[5] = [t1[1], t2[0], d3[1], t4[3], t5[1]]
    result[6] = [t1[1], t2[0], d3[1], t4[1], t5[3]]
    result[7] = [t1[1], t2[0], d3[1], t4[3], t5[3]]

    result[8] = [t1[1], t2[1], d3[2], t4[5], t5[1]]
    result[9] = [t1[1], t2[1], d3[2], t4[7], t5[1]]
    result[10] = [t1[1], t2[1], d3[2], t4[5], t5[3]]
    result[11] = [t1[1], t2[1], d3[2], t4[7], t5[3]]

    result[12] = [t1[0], t2[1], d3[3], t4[4], t5[0]]
    result[13] = [t1[0], t2[1], d3[3], t4[6], t5[0]]
    result[14] = [t1[0], t2[1], d3[3], t4[6], t5[2]]
    result[15] = [t1[0], t2[1], d3[3], t4[6], t5[2]]

    return result


""" datos = datos(x_euler, y_euler, z_euler, px, py, pz, l0, l1, l2, l4, l5)

Teta1 = teta1(datos)
Teta2 = teta2(datos, Teta1)
D3 = d3(datos, Teta1, Teta2)
Teta4 = teta4(datos, Teta1, Teta2)
Teta5 = teta5(datos, Teta1)
 """
""" print(r2d(Teta1))
print(r2d(Teta2))
print(D3)
print(r2d(Teta4))
print(r2d(Teta5))
print(datos.eulerxyz[0:-1,0:-1])
for x in range(0,16):
    print("-------------------------------------------------")
    print(lista(r2d(Teta1),r2d(Teta2),D3,r2d(Teta4),r2d(Teta5))[x]) """
