import operaciones as op
from scipy.spatial.transform import Rotation as R
import numpy as np

theta1 = 30
theta2 = 10
D3 = 14
theta4 = 20
theta5 = 20

l0 = 10
l1 = 12
l2 = 12
l4 = 13
l5 = 15


def htm(theta1, theta2, theta4, theta5, l0, l1, l2, D3, l4, l5):
    A1 = op.rotate('z', theta1, 'degrees') * op.translate('z',
                                                          l0+l1) * op.rotate('x', 90, 'degrees')
    A2 = op.rotate('z', theta2 + 90, 'degrees') * \
        op.translate('z', l2) * op.rotate('x', 90, 'degrees')
    A3 = op.rotate('z', 180, 'degrees') * op.translate('z',
                                                       D3) * op.rotate('x', 90, 'degrees')
    A4 = op.rotate('z', theta4 + 180, 'degrees') * \
        op.rotate('x', 90, 'degrees')
    A5 = op.rotate('z', theta5 + 180, 'degrees') * op.translate('z', l4+l5)
    return A1*A2*A3*A4*A5


def matrix_to_euler_xyz(matriz):
    r = R.from_matrix(matriz[0:-1, 0:-1])
    euler = r.as_euler('zyx', degrees=True)
    return np.flip(euler)


Resultado = htm(theta1, theta2, theta4, theta5, l0, l1, l2, D3, l4, l5)
euler = matrix_to_euler_xyz(Resultado)

""" print(Resultado.tolist()) """
# print(euler)
