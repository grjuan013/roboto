import sys
from PyQt5.QtCore import QPoint, QSize, Qt, QTime, QTimer
from ui_secundaria import *
from main import *
from texttable import *

import inverso as inv
import directo as dire


class Second(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_secondWindow()
        self.ui.setupUi(self)

        self.setWindowTitle("ROBOTO")
        self.setWindowIcon(QtGui.QIcon(
            "C:/Users/PC/Desktop/ROBOTO/icons/cil-browser.png"))

        self.ui.lineEdit_theta1.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_theta2.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_d3.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_theta4.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_theta5.setValidator(QtGui.QDoubleValidator())

        self.ui.lineEdit_yaw.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_pitch.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_roll.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_px.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_py.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_pz.setValidator(QtGui.QDoubleValidator())

        self.ui.radioButton_2.setChecked(False)
        self.ui.lineEdit_yaw.setEnabled(False)
        self.ui.lineEdit_pitch.setEnabled(False)
        self.ui.lineEdit_roll.setEnabled(False)
        self.ui.lineEdit_px.setEnabled(False)
        self.ui.lineEdit_py.setEnabled(False)
        self.ui.lineEdit_pz.setEnabled(False)

        self.ui.pushButton_combinaciones.setEnabled(False)

        def moveWindow(event):
            # MOVE WINDOW
            if event.buttons() == Qt.LeftButton:
                self.move(self.pos() + event.globalPos() - self.dragPos)
                self.dragPos = event.globalPos()
                event.accept()

        # WIDGET TO MOVE
        self.ui.frame_titulo.mouseMoveEvent = moveWindow

        """ self.ui.lineEdit_yaw.setText("-40.89339465")
        self.ui.lineEdit_pitch.setText("48.59037789")
        self.ui.lineEdit_roll.setText("69.10660535")
        self.ui.lineEdit_px.setText("38.94015945")
        self.ui.lineEdit_py.setText("8.62570508")
        self.ui.lineEdit_pz.setText("38.43107449") """

        """ self.ui.lineEdit_theta1.setText("30")
        self.ui.lineEdit_theta2.setText("10")
        self.ui.lineEdit_d3.setText("14")
        self.ui.lineEdit_theta4.setText("20")
        self.ui.lineEdit_theta5.setText("20") """

        self.ui.btn_close.clicked.connect(lambda: self.close())
        self.ui.btn_minimize.clicked.connect(lambda: self.showMinimized())

        def on_rbDirecto_checked():
            self.ui.radioButton_2.setChecked(False)
            self.ui.lineEdit_yaw.setEnabled(False)
            self.ui.lineEdit_pitch.setEnabled(False)
            self.ui.lineEdit_roll.setEnabled(False)
            self.ui.lineEdit_px.setEnabled(False)
            self.ui.lineEdit_py.setEnabled(False)
            self.ui.lineEdit_pz.setEnabled(False)

            self.ui.pushButton_combinaciones.setEnabled(False)

            self.ui.radioButton.setChecked(True)
            self.ui.lineEdit_theta1.setEnabled(True)
            self.ui.lineEdit_theta2.setEnabled(True)
            self.ui.lineEdit_d3.setEnabled(True)
            self.ui.lineEdit_theta4.setEnabled(True)
            self.ui.lineEdit_theta5.setEnabled(True)

        def on_rbInverso_checked():
            self.ui.radioButton.setChecked(False)
            self.ui.lineEdit_theta1.setEnabled(False)
            self.ui.lineEdit_theta2.setEnabled(False)
            self.ui.lineEdit_d3.setEnabled(False)
            self.ui.lineEdit_theta4.setEnabled(False)
            self.ui.lineEdit_theta5.setEnabled(False)

            self.ui.pushButton_combinaciones.setEnabled(True)

            self.ui.radioButton_2.setChecked(True)
            self.ui.lineEdit_yaw.setEnabled(True)
            self.ui.lineEdit_pitch.setEnabled(True)
            self.ui.lineEdit_roll.setEnabled(True)
            self.ui.lineEdit_px.setEnabled(True)
            self.ui.lineEdit_py.setEnabled(True)
            self.ui.lineEdit_pz.setEnabled(True)

        self.ui.radioButton.clicked.connect(on_rbDirecto_checked)
        self.ui.radioButton_2.clicked.connect(on_rbInverso_checked)

        def on_btnLimpiar_clicked():
            self.ui.lineEdit_theta1.clear()
            self.ui.lineEdit_theta2.clear()
            self.ui.lineEdit_d3.clear()
            self.ui.lineEdit_theta4.clear()
            self.ui.lineEdit_theta5.clear()

            self.ui.lineEdit_yaw.clear()
            self.ui.lineEdit_pitch.clear()
            self.ui.lineEdit_roll.clear()
            self.ui.lineEdit_px.clear()
            self.ui.lineEdit_py.clear()
            self.ui.lineEdit_pz.clear()
            self.ui.plainTextEdit_result.clear()

        self.ui.pushButton_limpiar.clicked.connect(on_btnLimpiar_clicked)

        def on_btnAceptar_clicked():
            self.ui.plainTextEdit_result.clear()
            if self.ui.radioButton.isChecked() and self.ui.lineEdit_theta1.text() and self.ui.lineEdit_theta2.text() and self.ui.lineEdit_theta4.text() and self.ui.lineEdit_theta5.text() and self.ui.lineEdit_d3.text() and self.ui.lineEdit_l1.text() and self.ui.lineEdit_l2.text() and self.ui.lineEdit_l4.text() and self.ui.lineEdit_l5.text():
                """ print("Directo") """
                resultado = dire.htm(float(self.ui.lineEdit_theta1.text()), float(self.ui.lineEdit_theta2.text()), float(self.ui.lineEdit_theta4.text()),
                                     float(self.ui.lineEdit_theta5.text()), float(self.ui.lineEdit_l0.text()), float(self.ui.lineEdit_l1.text()), float(self.ui.lineEdit_l2.text()), float(self.ui.lineEdit_d3.text()), float(self.ui.lineEdit_l4.text()), float(self.ui.lineEdit_l5.text()))

                euler = dire.matrix_to_euler_xyz(resultado)

                table = Texttable()
                table.set_deco(Texttable.HEADER)
                table.set_cols_dtype(['f', 'f', 'f', 'f'])
                table.set_cols_align(["l", "l", "l", "l"])
                table.set_precision(10)
                for row in resultado.tolist():
                    table.add_row(row)
                table.set_cols_width([20, 20, 20, 20])

                """ print(resultado) """
                self.ui.plainTextEdit_result.insertPlainText(
                    table.draw() + "\n\n")
                # euler
                self.ui.plainTextEdit_result.insertPlainText(
                    "Yaw = " + str(euler[0]) + "   Pitch = " + str(euler[1]) + "   \nRoll = " + str(euler[2]))

            elif self.ui.radioButton_2.isChecked() and self.ui.lineEdit_yaw.text() and self.ui.lineEdit_pitch.text() and self.ui.lineEdit_roll.text() and self.ui.lineEdit_px.text() and self.ui.lineEdit_py.text() and self.ui.lineEdit_pz.text() and self.ui.lineEdit_l1.text() and self.ui.lineEdit_l2.text() and self.ui.lineEdit_l4.text() and self.ui.lineEdit_l5.text():
                """ print("Inverso") """
                datos = inv.datos(float(self.ui.lineEdit_yaw.text()), float(self.ui.lineEdit_pitch.text()), float(self.ui.lineEdit_roll.text()), float(self.ui.lineEdit_px.text()), float(self.ui.lineEdit_py.text()), float(
                    self.ui.lineEdit_pz.text()), float(self.ui.lineEdit_l0.text()), float(self.ui.lineEdit_l1.text()), float(self.ui.lineEdit_l2.text()), float(self.ui.lineEdit_l4.text()), float(self.ui.lineEdit_l5.text()))
                Teta1 = inv.teta1(datos)
                Teta2 = inv.teta2(datos, Teta1)
                D3 = inv.d3(datos, Teta1, Teta2)
                Teta4 = inv.teta4(datos, Teta1, Teta2)
                Teta5 = inv.teta5(datos, Teta1)

                self.ui.plainTextEdit_result.insertPlainText(
                    "θ1: "+str(inv.r2d(Teta1)) + "\n" + "θ2: " + str(inv.r2d(Teta2)) + "\n" +
                    "D3: "+str(D3) + "\n" + "θ4: " + str(inv.r2d(Teta4)) +
                    "\n" + "θ5: "+str(inv.r2d(Teta5)))
            else:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setStyleSheet("background-color: rgb(56, 65, 85);")
                msg.setText(
                    "¡Error! \nSeleccione una opción, sea Directo o Inverso e ingrese los valores.")
                msg.setWindowTitle("Error")
                msg.exec_()

        self.ui.pushButton_aceptar.clicked.connect(on_btnAceptar_clicked)

        def on_btnCombinaciones_clicked():
            if self.ui.radioButton_2.isChecked() and self.ui.lineEdit_yaw.text() and self.ui.lineEdit_pitch.text() and self.ui.lineEdit_roll.text() and self.ui.lineEdit_px.text() and self.ui.lineEdit_py.text() and self.ui.lineEdit_pz.text() and self.ui.lineEdit_l1.text() and self.ui.lineEdit_l2.text() and self.ui.lineEdit_l4.text() and self.ui.lineEdit_l5.text():
                self.ui.plainTextEdit_result.clear()
                datos = inv.datos(float(self.ui.lineEdit_yaw.text()), float(self.ui.lineEdit_pitch.text()), float(self.ui.lineEdit_roll.text()), float(self.ui.lineEdit_px.text()), float(self.ui.lineEdit_py.text()), float(
                    self.ui.lineEdit_pz.text()), float(self.ui.lineEdit_l0.text()), float(self.ui.lineEdit_l1.text()), float(self.ui.lineEdit_l2.text()), float(self.ui.lineEdit_l4.text()), float(self.ui.lineEdit_l5.text()))
                Teta1 = inv.teta1(datos)
                Teta2 = inv.teta2(datos, Teta1)
                D3 = inv.d3(datos, Teta1, Teta2)
                Teta4 = inv.teta4(datos, Teta1, Teta2)
                Teta5 = inv.teta5(datos, Teta1)

                comb = inv.lista(Teta1, Teta2, D3, Teta4, Teta5)
                # print(comb)
                for key in comb:
                    """ print(comb[key]) """
                    self.ui.plainTextEdit_result.appendPlainText(
                        str(comb[key]))
                    self.ui.plainTextEdit_result.appendPlainText(
                        "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
            else:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setStyleSheet("background-color: rgb(56, 65, 85);")
                msg.setText(
                    "¡Error! \nSeleccione una opción, sea Directo o Inverso e ingrese los valores.")
                msg.setWindowTitle("Error")
                msg.exec_()

        self.ui.pushButton_combinaciones.clicked.connect(
            on_btnCombinaciones_clicked)

    def mousePressEvent(self, event):
        # ###############################################
        # Get the current position of the mouse
        self.dragPos = event.globalPos()
