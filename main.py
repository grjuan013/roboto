import sys
from PyQt5.QtCore import QPoint, QSize, QTime, QTimer
from PyQt5.QtWidgets import QMessageBox
from PySide2.QtCore import (Qt, QEvent)
from ui_principal import *
from second import *


class Main(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_Principal()
        self.ui.setupUi(self)

        self.setWindowTitle("ROBOTO")
        self.setWindowIcon(QtGui.QIcon(
            "C:/Users/PC/Desktop/ROBOTO/icons/cil-browser.png"))

        self.ui.lineEdit_l0.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_l1.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_l2.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_l4.setValidator(QtGui.QDoubleValidator())
        self.ui.lineEdit_l5.setValidator(QtGui.QDoubleValidator())

        """ self.ui.lineEdit_l0.setText("10")
        self.ui.lineEdit_l1.setText("12")
        self.ui.lineEdit_l2.setText("12")
        self.ui.lineEdit_l4.setText("13")
        self.ui.lineEdit_l5.setText("15") """

        self.ui.btn_close.clicked.connect(lambda: self.close())
        self.ui.btn_minimize.clicked.connect(lambda: self.showMinimized())

        def moveWindow(event):
            # MOVE WINDOW
            if event.buttons() == Qt.LeftButton:
                self.move(self.pos() + event.globalPos() - self.dragPos)
                self.dragPos = event.globalPos()
                event.accept()

        # WIDGET TO MOVE
        self.ui.frame_titulo.mouseMoveEvent = moveWindow

        def btn_ok_clicked():
            if self.ui.lineEdit_l0.text() and self.ui.lineEdit_l1.text() and self.ui.lineEdit_l2.text() and self.ui.lineEdit_l4.text() and self.ui.lineEdit_l5.text():
                self.secundaria = Second()
                self.secundaria.setWindowFlags(QtCore.Qt.FramelessWindowHint)
                self.secundaria.setAttribute(
                    QtCore.Qt.WA_TranslucentBackground)

                self.secundaria.ui.lineEdit_l0.setText(
                    self.ui.lineEdit_l0.text())
                self.secundaria.ui.lineEdit_l1.setText(
                    self.ui.lineEdit_l1.text())
                self.secundaria.ui.lineEdit_l2.setText(
                    self.ui.lineEdit_l2.text())
                self.secundaria.ui.lineEdit_l4.setText(
                    self.ui.lineEdit_l4.text())
                self.secundaria.ui.lineEdit_l5.setText(
                    self.ui.lineEdit_l5.text())
                self.secundaria.show()

                self.hide()
            else:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setStyleSheet("background-color: rgb(56, 65, 85);")
                msg.setText(
                    "¡Error! \nCampos obligatorios                     ")
                msg.setWindowTitle("Error")
                msg.exec_()

        self.ui.pushButton_ok.clicked.connect(btn_ok_clicked)

    def mousePressEvent(self, event):
        # ###############################################
        # Get the current position of the mouse
        self.dragPos = event.globalPos()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Main()
    window.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    window.setAttribute(QtCore.Qt.WA_TranslucentBackground)
    window.show()
    sys.exit(app.exec_())
